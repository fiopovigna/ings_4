<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Clase 3" >
    <title > Ejercicio 3 </title >
</head >
<body >
<?php

$a = [
        [
                'c1' => 'Coca Cola',
                'c2' => 100,
                'c3' => '4.500',
        ],
        [
                'c1' => 'Pepsi',
                'c2' => 30,
                'c3' => '4.800',
        ],
        [
                'c1' => 'Sprite',
                'c2' => 20,
                'c3' => '4.500',
        ],
        [
                'c1' => 'Guaraná',
                'c2' => 200,
                'c3' => '4.500',
        ],
        [
                'c1' => 'SevenUp',
                'c2' => 24,
                'c3' => '4.800',
        ],
        [
                'c1' => 'Mirinda Naranja',
                'c2' => 56,
                'c3' => '4.800',
        ],
        [
                'c1' => 'Mirinda Garaná',
                'c2' => 89,
                'c3' => '4.800',
        ],
        [
                'c1' => 'Fanta Nanranja',
                'c2' => 10,
                'c3' => '4.500',
        ],
        [
                'c1' => 'Fanta Piña',
                'c2' => 2,
                'c3' => '4.500',
        ],
    ];


    echo '<div>';
    $s = '<table>';
    $s .= '<tr class="productos"><th colspan="3"> Productos </th></tr>';
    $s .= '<tr class ="cabecera"><td > Nombre </td><td > Cantidad </td><td > Precio(Gs) </td></tr>';
         $count = 0;
foreach ( $a as $r ) {
        $s .= '<tr >';
        foreach ( $r as $v ) {
                $s .= '<td>'.$v.'</td>'; 
        }
        $s .= '</tr>';
}
$s .= '</table>';   
echo '<br>', $s;
echo '</div>';
  ?>

</body >

<style>
tr:nth-child(even){background:#E4E4E4;}
tr:nth-child(odd){background:#FFFFFF;}

.cabecera {
        background: #BFBFBF !important;
}

.productos {
        background:yellow !important;
}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
   text-align: center;

}
td {
  width: 125px;
}

</style>

</html >