<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Clase 4" >
    <title > Ejercicio 6</title >
</head >
<body >
    <?php
    /*
    6- Ejercicio 6:
Hacer un script PHP el cual utilice el operador ternario de PHP para realizar lo siguiente:
• Se deben declarar tres variables y le asignan valores enteros aleatorios (los valores deben
estar entre 99 y 999). Las variables serán $a, $b y $c
• Si la expresión $a*3 > $b+$c se debe imprimir que la expresión $a*3 es mayor que la
expresión $b+$c
• Si la expresión $a*3 <= $b+$c se debe imprimir que la expresión $b+$c es mayor o igual
que la expresión $a*3
    */ 
        $a =rand(99,999); 
        $b =rand(99,999);
        $c =rand(99,999);
        
        echo '<u>Variable A:</u> '.$a.'<br> <u>Variable B:</u> '.$b.'<br> <u>Variable C:</u> '.$c.'<br><br><br>';
       
        echo ($a*3) > ($b+$c) ? 'B+C es mayor o igual que la expresión A*3. <br><b> B+C: '.($b+$c).'</b>' : 'A*3 es mayor que la expresión B+C.<br> <b>A*3: '.($a*3).'</b>';



    ?>
</body >
</html >