<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Clase 4" >
    <title > Ejercicio 5</title >
</head >
<body >
   <?php
/*
5-Ejercicio 5:
• El script PHP debe estar embebido en una página HTML
• Hacer un script en PHP que muestre en pantalla la tabla de multiplicar el 9, coloreando las
filas alternando gris y blanco
*/
   echo '<b><u>Tabla del 9</u></b> <br>';

$a = [
        [
                'c1' => 9,
                'c2' => 'x',
                'c3' => 1,
                'c4' => '=',
                'c5' => 9,
        ],
        [
                'c1' => 9,
                'c2' => 'x',
                'c3' => 2,
                'c4' => '=',
                'c5' => 18,
        ],
        [
                'c1' => 9,
                'c2' => 'x',
                'c3' => 3,
                'c4' => '=',
                'c5' => 27,
        ],
        [
                'c1' => 9,
                'c2' => 'x',
                'c3' => 4,
                'c4' => '=',
                'c5' => 36,
        ],
        [
                'c1' => 9,
                'c2' => 'x',
                'c3' => 5,
                'c4' => '=',
                'c5' => 45,
        ],
        [
                'c1' => 9,
                'c2' => 'x',
                'c3' => 6,
                'c4' => '=',
                'c5' => 54,
        ],
        [
                'c1' => 9,
                'c2' => 'x',
                'c3' => 7,
                'c4' => '=',
                'c5' => 63,
        ],
        [
                'c1' => 9,
                'c2' => 'x',
                'c3' => 8,
                'c4' => '=',
                'c5' => 72,
        ],
        [
                'c1' => 9,
                'c2' => 'x',
                'c3' => 9,
                'c4' => '=',
                'c5' => 81,
        ],
        [
                'c1' => 9,
                'c2' => 'x',
                'c3' => 10,
                'c4' => '=',
                'c5' => 90,
        ],
    ];


    echo '<div>';
    $s = '<table>';
         $count = 0;
foreach ( $a as $r ) {
        $s .= '<tr >';
        foreach ( $r as $v ) {
                $s .= '<td>'.$v.'</td>'; 
        }
        $s .= '</tr>';
}
$s .= '</table>';   
echo '<br>', $s;
echo '</div>';
  ?>

</body >

<style>
tr:nth-child(even){background:grey;}
tr:nth-child(odd){background:white;}

table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
   text-align: center;

}
td {
  width: 25px;
}

</style>

</html >