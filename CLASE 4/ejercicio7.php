<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Clase 4" >
    <title > Ejercicio 7</title >
</head >

<body >
    <?php 
    /* 7- Ejercicio 7:
        Hacer un script PHP, utilizando la estructura de selección swicth que realice lo siguiente:
        • Se deben definir tres variables correspondientes a las notas de un alumno en un curso de
        PHP
        • La variable parcial1 puede tener un valor entre 0 y 30 (Se debe generar un valor aleatorio)
        • La variable parcial2 puede tener un valor entre 0 y 20 (Se debe generar un valor aleatorio)
        • La variable final1 puede tener un valor entre 0 y 50 (Se debe generar un valor aleatorio)
        Se deben sumar los tres acumulados (en la expresión del switch) e imprimir si el alumno tuvo nota
        1 o 2 o 3 o 4 o 5
        Nota 1: entre 0 y 59
        Nota 2: entre 60 y 69
        Nota 3: entre 70 y 79
        Nota 4: entre 80 y 89
        Nota 5: entre 90 y 100    */

        $parcial1 =rand(0,30); 
        $parcial2 =rand(0,20);
        $final1 =rand(0,50);
        
        echo '<u>Parcial 1:</u> '.$parcial1.'<br> <u>Parcial 2:</u> '.$parcial2.'<br> <u>Final 1:</u> '.$final1.'<br><br>';

        $suma= $parcial1+$parcial2+$final1;
        switch ($suma) {
            case ($suma >= 0 && $suma <=59):
                echo '<b>Nota: 1</b>';
                break;
            case ($suma >= 60 && $suma <=69):
                echo '<b>Nota: 2</b>';
                break;
            case ($suma >= 70 && $suma <=79):
                echo '<b>Nota: 3</b>';
                break;
            case ($suma >= 80 && $suma <=89):
                echo '<b>Nota: 4</b>';
                break;
            case ($suma >= 90 && $suma <=100):
                echo '<b>Nota: 5</b>';
                break;
        }
       
    ?>
</body >
</html >