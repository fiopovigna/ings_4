<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Clase 5" >
    <title > Ejercicio 1 </title >
</head >
<body >
    <?php 
    /* --- Ejercicio 1:
    Hacer un script PHP que realice el siguiente cálculo
    x = ((A * ¶) + B) / (C*D) - Se debe calcular e imprimir el valor de x
        Donde:
            • A es la raiz cuadrada de 2
            • ¶ es el número PI
            • B es es la raíz cúbica de 3
            • C es la constante de Euler
            • D es la constante e
    Observación: Utilizar las constantes matemáticas definidas den la extensión math de PHP  */

    $A  = M_SQRT2;
    $PI = M_PI; 
    $B  = M_SQRT3;
    $C  = M_EULER;
    $D  = M_E;

    echo 'A: '.$A.'<br>PI: '.$PI.'<br>B: '.$B.'<br>C: '.$C.'<br>D: '.$D.'<br><br><br>';

    $X = (($A*$PI)+$B)/($C*$D);
    echo '((A * ¶) + B) / (C*D) = '.$X;
    ?>
</body >
</html >