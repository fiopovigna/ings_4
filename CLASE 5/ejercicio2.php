<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Clase 5" >
    <title > Ejercicio 1 </title >
</head >
<body >
    <?php 
    /*Ejercicio 2:
    Hacer un script PHP que imprime la siguiente información:
    • Versión de PHP utilizada.
    • El id de la versión de PHP.
    • El valor máximo soportado para enteros para esa versión.
    • Tamaño máximo del nombre de un archivo.
    • Versión del Sistema Operativo.
    • Símbolo correcto de 'Fin De Línea' para la plataforma en uso.
    • El include path por defecto.
    Observación: Ver las constantes predefinidas del núcleo de PHP */
    
    echo 'Versión de PHP utilizada: '. phpversion;


    ?>
</body >
</html >